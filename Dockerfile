FROM node:buster

WORKDIR /usr/src/app

COPY package.json ./

RUN yarn install

EXPOSE 3000

COPY . ./

RUN yarn run build

CMD ["node", "dist/index.js"]