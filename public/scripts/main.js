const movie = () => ({
  title: "",
  year: "",
  results: [],
  directories: true,
  loading: false,
  fetchDirs() {
    this.loading = true;
    const params = new URLSearchParams(
      Object.entries({
        year: this.year,
        title: this.title,
        limit: 10,
      })
    ).toString();

    window
      .fetch("/api/movie/search?" + params)
      .then((res) => res.json())
      .then((json) => {
        this.directories = true;
        this.results = json.payload;
      })
      .finally(() => (this.loading = false));
  },
  fetchSingle(href) {
    this.loading = true;
    const params = new URLSearchParams(
      Object.entries({
        href,
      })
    ).toString();

    window
      .fetch("/api/movie/files?" + params)
      .then((res) => res.json())
      .then((json) => {
        this.directories = false;
        this.results = json.payload;
      })
      .finally(() => (this.loading = false));
  },
});

const tv = () => ({
  title: "",
  results: [],
  directories: true,
  loading: false,
  fetchDirs() {
    this.loading = true;
    const params = new URLSearchParams(
      Object.entries({
        title: this.title,
        limit: 10,
      })
    ).toString();

    window
      .fetch("/api/tv/search?" + params)
      .then((res) => res.json())
      .then((json) => {
        this.directories = true;
        this.results = json.payload;
      })
      .finally(() => (this.loading = false));
  },
  fetchSingle(href) {
    this.loading = true;
    const params = new URLSearchParams(
      Object.entries({
        href,
      })
    ).toString();

    window
      .fetch("/api/tv/files?" + params)
      .then((res) => res.json())
      .then((json) => {
        this.directories = !json.payload[0].isFile;
        this.results = json.payload;
      })
      .finally(() => (this.loading = false));
  },
});
