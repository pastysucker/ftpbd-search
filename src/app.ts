import { httpListener } from "@marblejs/core";
import { api$, apiv2$, serveFile$, movie$, tv$ } from "./effects/";
import { cors$, logger$ } from "./middleware";

const listener = httpListener({
  effects: [api$, apiv2$, movie$, tv$, serveFile$],
  middlewares: [logger$, cors$],
});

export { listener };
