import {
  combineRoutes,
  HttpMiddlewareEffect,
  r,
  HttpEffectResponse,
} from "@marblejs/core";
import { requestValidator$ } from "@marblejs/middleware-io";
import * as o from "rxjs/operators";
import { movieApi$ } from "./movie";
import { tvApi$ } from "./tv";
import { pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import * as T from "fp-ts/Task";
import * as t from "io-ts";
import { observable as RX } from "fp-ts-rxjs";
import type { ResponseBody } from "../../lib/types";
import { getImagesForMovie } from "../../lib";

const cache$: HttpMiddlewareEffect = (req$) =>
  req$.pipe(
    o.map((r) => {
      r.response.setHeader("Cache-Control", "max-age=35000");
      return r;
    })
  );

const imagesQuerySchema = t.type({
  name: t.string,
  year: t.string,
});
type ImagesQuery = t.TypeOf<typeof imagesQuerySchema>;

const images$ = r.pipe(
  r.matchPath("/images"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ name, year }: ImagesQuery) =>
      pipe(
        getImagesForMovie(name, year),
        TE.fold<Error, unknown, HttpEffectResponse<ResponseBody>>(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: imagesQuerySchema }),
      RX.chain(({ query }) => RX.fromTask(tx(query)))
    );
  })
);

export const api$ = combineRoutes("/api", {
  // middlewares: [cache$],
  effects: [movieApi$, tvApi$, images$],
});
