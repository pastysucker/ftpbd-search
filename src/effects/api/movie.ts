import { r, combineRoutes, HttpEffectResponse } from "@marblejs/core";
import { requestValidator$, t } from "@marblejs/middleware-io";
import { DateFromISOString, fromNullable, NumberFromString } from "io-ts-types";
import {
  ResponseBody,
  getMovieFiles,
  DirObject,
  getRecentMovieUploads,
  filterMoviesByTitleFromYear,
  paginationSchema,
  filteringSchema,
  getImagesForMovie,
} from "../../lib";
import { observable as o } from "fp-ts-rxjs";
import { pipe } from "fp-ts/function";
import * as T from "fp-ts/Task";
import * as TE from "fp-ts/TaskEither";
import { filterResults, paginateResults } from "../../lib/common";

const searchQuerySchema = t.intersection([
  t.type({
    year: NumberFromString,
    title: t.string,
  }),
  paginationSchema,
  filteringSchema,
]);
type SearchQuery = t.TypeOf<typeof searchQuerySchema>;

const search$ = r.pipe(
  r.matchPath("/search"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({
      year,
      title,
      limit,
      skip,
      nodeType,
      resolution,
    }: SearchQuery) =>
      pipe(
        filterMoviesByTitleFromYear(title, year),
        TE.map(filterResults({ nodeType, resolution })),
        TE.map(paginateResults({ limit, skip })),
        TE.fold(
          ({ message }) =>
            T.of<HttpEffectResponse<ResponseBody<DirObject[]>>>({
              body: { error: { message, status: 500 } },
              status: 500,
            }),
          (payload) =>
            T.of<HttpEffectResponse<ResponseBody<DirObject[]>>>({
              body: { payload },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ query: searchQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

const movieQuerySchema = t.intersection([
  t.type({
    href: t.string,
  }),
  paginationSchema,
  filteringSchema,
]);

type MovieQuery = t.TypeOf<typeof movieQuerySchema>;

const getByHref$ = r.pipe(
  r.matchPath("/files"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ href, nodeType, resolution, skip, limit }: MovieQuery) =>
      pipe(
        getMovieFiles(href),
        TE.map(filterResults({ nodeType, resolution })),
        // TE.map(paginateResults({ limit, skip })),
        TE.fold<
          Error,
          DirObject[],
          HttpEffectResponse<ResponseBody<DirObject[]>>
        >(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: movieQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

const monthDate = new Date();
monthDate.setDate(monthDate.getDate() - 30);
const recentQuerySchema = t.intersection([
  t.type({
    thresholdDate: fromNullable(DateFromISOString, monthDate),
  }),
  paginationSchema,
  filteringSchema,
]);

type RecentQuery = t.TypeOf<typeof recentQuerySchema>;

const recentUploads$ = r.pipe(
  r.matchPath("/recent"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({
      thresholdDate,
      limit,
      skip,
      nodeType,
      resolution,
    }: RecentQuery) =>
      pipe(
        getRecentMovieUploads(thresholdDate),
        TE.map(filterResults({ nodeType, resolution })),
        TE.map(paginateResults({ limit, skip })),
        TE.fold<
          Error,
          DirObject[],
          HttpEffectResponse<ResponseBody<DirObject[]>>
        >(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: recentQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

export const movieApi$ = combineRoutes("/movie", [
  search$,
  getByHref$,
  recentUploads$,
]);
