import { r, combineRoutes, HttpEffectResponse } from "@marblejs/core";
import { requestValidator$, t } from "@marblejs/middleware-io";
import {
  ResponseBody,
  getTvFiles,
  filterSeriesByTitle,
  DirObject,
  getRecentTvUploads,
  paginationSchema,
  filteringSchema,
} from "../../lib";
import { observable as o } from "fp-ts-rxjs";
import { pipe } from "fp-ts/function";
import * as T from "fp-ts/Task";
import * as TE from "fp-ts/TaskEither";
import { DateFromISOString, fromNullable, NumberFromString } from "io-ts-types";
import { filterResults, paginateResults } from "../../lib/common";

const searchQuerySchema = t.intersection([
  t.type({
    title: t.string,
  }),
  paginationSchema,
  filteringSchema,
]);

type SearchQuery = t.TypeOf<typeof searchQuerySchema>;

const search$ = r.pipe(
  r.matchPath("/search"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ title, limit, skip, nodeType, resolution }: SearchQuery) =>
      pipe(
        filterSeriesByTitle(title),
        TE.map(filterResults({ nodeType, resolution })),
        TE.map(paginateResults({ limit, skip })),
        TE.fold<
          Error,
          DirObject[],
          HttpEffectResponse<ResponseBody<DirObject[]>>
        >(
          ({ message }) =>
            T.of({
              body: { error: { message, status: 400 } },
              status: 400,
            }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: searchQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

const tvQuerySchema = t.intersection([
  t.type({
    href: t.string,
  }),
  paginationSchema,
  filteringSchema,
]);

type TvQuery = t.TypeOf<typeof tvQuerySchema>;

const getByHref$ = r.pipe(
  r.matchPath("/files"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ href, limit, skip, nodeType, resolution }: TvQuery) =>
      pipe(
        getTvFiles(href),
        TE.map(filterResults({ nodeType, resolution })),
        // TE.map(paginateResults({ limit, skip })),
        TE.fold<
          Error,
          DirObject[],
          HttpEffectResponse<ResponseBody<DirObject[]>>
        >(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: tvQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

const twoMonths = new Date();
twoMonths.setDate(twoMonths.getDate() - 60);
const recentQuerySchema = t.intersection([
  t.type({
    thresholdDate: fromNullable(DateFromISOString, twoMonths),
  }),
  filteringSchema,
  paginationSchema,
]);

type RecentQuery = t.TypeOf<typeof recentQuerySchema>;

const recentUploads$ = r.pipe(
  r.matchPath("/recent"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({
      thresholdDate,
      limit,
      skip,
      nodeType,
      resolution,
    }: RecentQuery) =>
      pipe(
        getRecentTvUploads(thresholdDate),
        TE.map(filterResults({ nodeType, resolution })),
        TE.map(paginateResults({ limit, skip })),
        TE.fold<
          Error,
          DirObject[],
          HttpEffectResponse<ResponseBody<DirObject[]>>
        >(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) => T.of({ body: { payload } })
        )
      );

    return req$.pipe(
      requestValidator$({ query: recentQuerySchema }),
      o.chain(({ query }) => o.fromTask(tx(query)))
    );
  })
);

export const tvApi$ = combineRoutes("/tv", [
  search$,
  getByHref$,
  recentUploads$,
]);
