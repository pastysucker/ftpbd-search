import { movieApi$ } from "./movie";
import { seriesApi$ } from "./series";
import { r, combineRoutes, HttpEffectResponse } from "@marblejs/core";
import { requestValidator$, t } from "@marblejs/middleware-io";
import * as TE from "fp-ts/TaskEither";
import * as T from "fp-ts/Task";
import { pipe } from "fp-ts/function";
import { observable as RX } from "fp-ts-rxjs";
import { MediaSource, ResponseBody } from "../../../lib";
import { getPlaybackInfo } from "../../../lib/emby/v2";
import { jsonUtf8 } from "../../../middleware";

const sourcesParamSchema = t.type({
  id: t.string,
});
type SourcesParam = t.TypeOf<typeof sourcesParamSchema>;
const getSources$ = r.pipe(
  r.matchPath("/sources/:id"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ id }: SourcesParam) =>
      pipe(
        getPlaybackInfo(id),
        TE.fold<
          Error,
          MediaSource[],
          HttpEffectResponse<ResponseBody<MediaSource[]>>
        >(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );
    return req$.pipe(
      requestValidator$({ params: sourcesParamSchema }),
      RX.chain(({ params }) => RX.fromTask(tx(params)))
    );
  })
);

export const api$ = combineRoutes("/api/v2", {
  middlewares: [jsonUtf8],
  effects: [movieApi$, seriesApi$, getSources$],
});

export { wsListener } from "./socket";
