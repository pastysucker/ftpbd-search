import { r, combineRoutes, HttpEffectResponse } from "@marblejs/core";
import { requestValidator$, t } from "@marblejs/middleware-io";
import * as TE from "fp-ts/TaskEither";
import * as T from "fp-ts/Task";
import { pipe } from "fp-ts/function";
import { observable as RX } from "fp-ts-rxjs";
import { getLatestItems, getMovie, searchItems } from "../../../lib/emby/v2";
import {
  Movie,
  paginationSchema,
  ResponseBody,
  SearchResult,
} from "../../../lib";
import { CollectionId } from "../../../lib/emby/constants";

const movieParamSchema = t.type({
  id: t.string,
});

type MovieParams = t.TypeOf<typeof movieParamSchema>;

const getById$ = r.pipe(
  r.matchPath("/:id"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ id }: MovieParams) =>
      pipe(
        getMovie(id),
        TE.fold<Error, Movie, HttpEffectResponse<ResponseBody<Movie>>>(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ params: movieParamSchema }),
      RX.chain(({ params }) => RX.fromTask(tx(params)))
    );
  })
);

const searchQuerySchema = t.intersection([
  t.type({
    query: t.string,
  }),
  paginationSchema,
]);

type SearchQuery = t.TypeOf<typeof searchQuerySchema>;

const search$ = r.pipe(
  r.matchPath("/search"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ query, limit, skip }: SearchQuery) =>
      pipe(
        searchItems(query, "Movie", limit, skip),
        TE.fold<
          Error,
          SearchResult[],
          HttpEffectResponse<ResponseBody<SearchResult[]>>
        >(
          ({ message, name }) => {
            console.error(name, message);
            return T.of({
              body: { error: { message, status: 400 } },
              status: 400,
            });
          },
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ query: searchQuerySchema }),
      RX.chain(({ query }) => RX.fromTask(tx(query)))
    );
  })
);

const latestQuerySchema = paginationSchema;
type LatestQuery = t.TypeOf<typeof latestQuerySchema>;
const latestMovies$ = r.pipe(
  r.matchPath("/latest"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ limit, skip }: LatestQuery) =>
      pipe(
        getLatestItems("Movie", CollectionId.EnglishMovies, limit, skip),
        TE.fold<
          Error,
          SearchResult[],
          HttpEffectResponse<ResponseBody<SearchResult[]>>
        >(
          ({ message, name }) => {
            console.error(name, message);
            return T.of({
              body: { error: { message, status: 400 } },
              status: 400,
            });
          },
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ query: latestQuerySchema }),
      RX.chain(({ query }) => RX.fromTask(tx(query)))
    );
  })
);

export const movieApi$ = combineRoutes("/movie", [
  latestMovies$,
  search$,
  getById$,
]);
