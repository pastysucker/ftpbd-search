import { r, combineRoutes, HttpEffectResponse } from "@marblejs/core";
import { requestValidator$, t } from "@marblejs/middleware-io";
import * as TE from "fp-ts/TaskEither";
import * as T from "fp-ts/Task";
import { pipe } from "fp-ts/function";
import { observable as RX } from "fp-ts-rxjs";
import {
  getEpisodes,
  getLatestItems,
  getSeasons,
  getSeries,
  searchItems,
} from "../../../lib/emby/v2";
import {
  Episode,
  paginationSchema,
  ResponseBody,
  SearchResult,
  Season,
  Series,
} from "../../../lib";
import { CollectionId } from "../../../lib/emby/constants";

const seriesParamSchema = t.type({
  id: t.string,
});
type SeriesParam = t.TypeOf<typeof seriesParamSchema>;
const getById$ = r.pipe(
  r.matchPath("/:id"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ id }: SeriesParam) =>
      pipe(
        getSeries(id),
        TE.fold<Error, Series, HttpEffectResponse<ResponseBody<Series>>>(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ params: seriesParamSchema }),
      RX.chain(({ params }) => RX.fromTask(tx(params)))
    );
  })
);

const seasonsParamSchema = t.type({
  id: t.string,
});
type SeasonsParam = t.TypeOf<typeof seasonsParamSchema>;
const getSeasons$ = r.pipe(
  r.matchPath("/:id/seasons"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ id }: SeasonsParam) =>
      pipe(
        getSeasons(id),
        TE.fold<Error, Season[], HttpEffectResponse<ResponseBody<Season[]>>>(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );
    return req$.pipe(
      requestValidator$({ params: seasonsParamSchema }),
      RX.chain(({ params }) => RX.fromTask(tx(params)))
    );
  })
);

const episodesParamSchema = t.type({
  id: t.string,
  seasonId: t.string,
});
type EpisodesParam = t.TypeOf<typeof episodesParamSchema>;
const getEpisodes$ = r.pipe(
  r.matchPath("/:id/seasons/:seasonId/episodes"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ id, seasonId }: EpisodesParam) =>
      pipe(
        getEpisodes(id, seasonId),
        TE.fold<Error, Episode[], HttpEffectResponse<ResponseBody<Episode[]>>>(
          ({ message }) =>
            T.of({ body: { error: { message, status: 400 } }, status: 400 }),
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );
    return req$.pipe(
      requestValidator$({ params: episodesParamSchema }),
      RX.chain(({ params }) => RX.fromTask(tx(params)))
    );
  })
);

const searchQuerySchema = t.type({
  query: t.string,
});
type SearchQuery = t.TypeOf<typeof searchQuerySchema>;
const search$ = r.pipe(
  r.matchPath("/search"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ query }: SearchQuery) =>
      pipe(
        searchItems(query, "Series"),
        TE.fold<
          Error,
          SearchResult[],
          HttpEffectResponse<ResponseBody<SearchResult[]>>
        >(
          ({ message, name }) => {
            console.error(name, message);
            return T.of({
              body: { error: { message, status: 400 } },
              status: 400,
            });
          },
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ query: searchQuerySchema }),
      RX.chain(({ query }) => RX.fromTask(tx(query)))
    );
  })
);

const latestQuerySchema = paginationSchema;
type LatestQuery = t.TypeOf<typeof latestQuerySchema>;
const latestSeries$ = r.pipe(
  r.matchPath("/latest"),
  r.matchType("GET"),
  r.useEffect((req$) => {
    const tx = ({ limit, skip }: LatestQuery) =>
      pipe(
        getLatestItems("Series", CollectionId.EnglishSeries, limit, skip),
        TE.fold<
          Error,
          SearchResult[],
          HttpEffectResponse<ResponseBody<SearchResult[]>>
        >(
          ({ message, name }) => {
            console.error(name, message);
            return T.of({
              body: { error: { message, status: 400 } },
              status: 400,
            });
          },
          (payload) =>
            T.of({
              body: { payload },
              headers: { "content-type": "application/json; charset=UTF-8" },
            })
        )
      );

    return req$.pipe(
      requestValidator$({ query: latestQuerySchema }),
      RX.chain(({ query }) => RX.fromTask(tx(query)))
    );
  })
);

export const seriesApi$ = combineRoutes("/series", [
  latestSeries$,
  search$,
  getById$,
  getEpisodes$,
  getSeasons$,
]);
