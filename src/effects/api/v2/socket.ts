import { matchEvent, isConnectEvent, ServerEvent } from "@marblejs/core";
import {
  ServerEventType,
  webSocketListener,
  WsEffect,
} from "@marblejs/websockets";
import * as o from "rxjs/operators";

const clients = [];

export const echo$: WsEffect = (event$) =>
  event$.pipe(
    matchEvent("INPUT"),
    o.map((e) => ({ type: "OUTPUT", payload: e.payload }))
  );

export const wsListener = webSocketListener({
  effects: [echo$],
});
