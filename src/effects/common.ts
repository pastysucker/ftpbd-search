import { HttpEffect, HttpError, HttpStatus, r } from "@marblejs/core";
import { of, iif, throwError } from "rxjs";
import * as o from "rxjs/operators";
import { readFile } from "../lib";
import { requestValidator$, t } from "@marblejs/middleware-io";

const validator$ = requestValidator$({
  params: t.type({
    dir: t.string,
  }),
});

const getFileEffect$: HttpEffect = (req$) =>
  req$.pipe(
    validator$,
    o.mergeMap((req) =>
      of(req.params.dir).pipe(
        o.mergeMap(readFile("./public")),
        o.map((body) => ({ body })),
        o.catchError((error) =>
          iif(
            () => error.code === "ENOENT",
            throwError(
              new HttpError(
                `Route not found for path: ${req.url}`,
                HttpStatus.NOT_FOUND
              )
            ),
            throwError(
              new HttpError(
                "Internal server error",
                HttpStatus.INTERNAL_SERVER_ERROR
              )
            )
          )
        )
      )
    )
  );

export const serveFile$ = r.pipe(
  r.matchPath("/:dir*"),
  r.matchType("GET"),
  r.useEffect(getFileEffect$)
);
