import { r } from "@marblejs/core";
import * as o from "rxjs/operators";
import * as pug from "pug";

export const movie$ = r.pipe(
  r.matchPath("/"),
  r.matchType("GET"),
  r.useEffect((req$) =>
    req$.pipe(
      o.mapTo({
        headers: {
          "Content-Type": "text/html",
        },
        body: pug.renderFile("./src/views/index.pug"),
      })
    )
  )
);

export const tv$ = r.pipe(
  r.matchPath("/tv"),
  r.matchType("GET"),
  r.useEffect((req$) =>
    req$.pipe(
      o.mapTo({
        headers: {
          "Content-Type": "text/html",
        },
        body: pug.renderFile("./src/views/tv.pug"),
      })
    )
  )
);
