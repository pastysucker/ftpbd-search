import { createServer } from "@marblejs/core";
import { IO } from "fp-ts/IO";
import { listener as restListener } from "./app";

const restServer = createServer({
  port: 6565,
  hostname: "0.0.0.0",
  listener: restListener,
});

// const wsServer = createWebSocketServer({
//   options: {
//     port: 1337,
//     host: "0.0.0.0",
//   },
//   listener: wsListener,
// });

const rest: IO<void> = async () => restServer.then(async (e) => await e());
// const ws: IO<void> = async () => await (await wsServer)();

rest();
// ws();
