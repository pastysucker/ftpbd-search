import { test, expect } from "@jest/globals";
import { parseTitle } from "../folderNameParser";

test("should parse folder name correctly", () => {
  const case1 = "Irrational Man 2015 720p",
    case2 = "The Martian (2015)",
    case3 = "2 Broke Girls - Tv Series 2014 - 720p";

  expect(parseTitle(case1)).toBe("Irrational Man");
  expect(parseTitle(case2)).toBe("The Martian");
  expect(parseTitle(case3)).toBe("2 Broke Girls");
});
