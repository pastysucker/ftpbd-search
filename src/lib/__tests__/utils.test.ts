import { expect, test } from "@jest/globals";
import { isFile, kbToBytes } from "../utils";

test("should match filename", () => {
  const cases = [
    "Austin Powers-3 (in Goldmember) [2002].mkv",
    "Barbershop.2002.1080p.BluRay.x264-[YTS.AM].mp4",
    "Catch.Me.If.You.Can.2002.1080p.BluRay.x264.YIFY.mp4",
  ];
  for (const i of cases) {
    expect(isFile(i)).toBe(true);
  }
});

test("should convert KB to bytes", () => {
  const case1 = "1000 KB";

  expect(kbToBytes(case1)).toBe(1000000);
});
