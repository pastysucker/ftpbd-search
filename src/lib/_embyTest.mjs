import { fetch } from "cross-fetch";

/**
http://media.ftpbd.net:8096/emby/Users/1bbda9d8543e484499096a7feea95679/Items/192182?X-Emby-Client=Emby%20Web&X-Emby-Device-Name=Chrome&X-Emby-Device-Id=a795d87b-6c33-4de5-b530-b4c6413cdf7e&X-Emby-Client-Version=4.5.2.0&X-Emby-Token=180d3ec12eaa45759efae20504991461
*/

fetch(
  "http://media.ftpbd.net:8096/emby/Users/1bbda9d8543e484499096a7feea95679/Items?" +
    new URLSearchParams({
      Recursive: "true",
      Fields: "Path",
      // Path:
      //   "/bnet-server-2/FILE--SERVER/FTP-2/English Movies/2010/Inception (2010) 1080p/",
      EnableImages: "true",
      IncludeItemTypes: "Series",
      NameStartsWith: "New Girl",
      Years: "2011",
    }),
  {
    headers: {
      "X-Emby-Client": "Emby Web",
      "X-Emby-Device-Name": "Chrome",
      "X-Emby-Device-Id": "a795d87b-6c33-4de5-b530-b4c6413cdf7e",
      "X-Emby-Token": "180d3ec12eaa45759efae20504991461",
    },
  }
)
  .then((res) => res.json())
  .then((json) => {
    console.log(JSON.stringify(json, null, 2));
  });

// fetch(
//   "http://media.ftpbd.net:8096/emby/Items/188940/Images?" +
//     new URLSearchParams({
//       // tag: "9f2adce55ef4ff8ee588ce626458dac4",
//       // Recursive: true,
//       // Fields: "Path",
//       // Path:
//       //   "/FTP-2/English%20Movies/2015/Holding%20The%20Man%20%282015%29%201080p/",
//       // IncludeItemTypes: "Movie",
//       // NameLessThan: "Interstellar (2014)",
//     }),
//   {
//     headers: {
//       // "X-Emby-Client": "Emby Web",
//       // "X-Emby-Device-Name": "Chrome",
//       // "X-Emby-Device-Id": "a795d87b-6c33-4de5-b530-b4c6413cdf7e",
//       "X-Emby-Token": "180d3ec12eaa45759efae20504991461",
//     },
//   }
// )
//   .then((res) => {
//     // const t = res.headers.get("Content-Type");
//     // console.log(t);
//     // res.text().then(console.log);
//     return res.json();
//   })
//   .then((json) => {
//     console.log(JSON.stringify(json, null, 2));
//   });
