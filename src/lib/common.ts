import { filenameParse } from "@ctrl/video-filename-parser";
import cheerio from "cheerio";
import fetch from "cross-fetch";
import { Do } from "fp-ts-contrib/Do";
import * as A from "fp-ts/Array";
import { Ord as ordDate } from "fp-ts/Date";
import * as E from "fp-ts/Either";
import { pipe } from "fp-ts/function";
import { contramap, fromCompare, Ord, reverse } from "fp-ts/Ord";
import * as TE from "fp-ts/TaskEither";
import { folderParse } from "./folderNameParser";
import { diceSimilarity } from "./stringSimilarity";
import { DirObject, FilteringOptions, PaginationOptions } from "./types";
import { isFile, kbToBytes } from "./utils";
import * as emby from "./emby";

export const englishMovieDomain = "http://server2.ftpbd.net";
export const englishSeriesDomain = "http://server4.ftpbd.net";

type Domain = typeof englishMovieDomain | typeof englishSeriesDomain;

export const tap = <T>(fun: (val: T) => void) => (val: T) => {
  fun(val);
  return val;
};

export const getParsedDoc = (url: URL) => {
  return pipe(
    TE.tryCatch(
      () =>
        fetch(url.href).then((res) => {
          if (!res.ok) throw new Error("Failed with status: " + res.status);
          return res.text();
        }),
      (e) => {
        console.error(e);
        if (e instanceof Error) {
          return e;
        } else
          return new Error(
            "Error while fetching. Check your queries and try again."
          );
      }
    ),
    TE.chain((body) => {
      const $ = cheerio.load(body);
      return pipe(
        $("table tr:not(:first-child):not(:nth-child(2))"),
        E.fromNullable(new Error("Selector invalid")),
        TE.fromEither
      );
    })
  );
};

/**
 * Returns array containing name and href of files in directory
 */
export const getNodes = (url: URL, type: "tv" | "movie", extras = false) => {
  // bind is akin to chain
  // let is same as map
  // do is for side effects
  // L-suffixed versions are lazy

  return Do(TE.Monad)
    .bind("parsedDoc", getParsedDoc(url))
    .letL("files", ({ parsedDoc }) =>
      parsedDoc
        .map<DirObject>((_i, e) => {
          const fbn = cheerio(".fb-n > a", e); // name
          const fbs = cheerio(".fb-s", e); // size
          const fbd = cheerio(".fb-d", e); // date

          const name = fbn.text();
          const getIsFile = isFile(name);
          const fileSize = kbToBytes(fbs.text());
          const date = new Date(fbd.text());
          const href = fbn.attr("href");
          const fullHref = getIsFile ? url.origin + href : href;

          return {
            name,
            href: fullHref ?? "/",
            isFile: getIsFile,
            lastModified: date,
            fileSize: getIsFile ? fileSize : undefined,
            meta: getIsFile
              ? filenameParse(name, type === "tv")
              : folderParse(name),
          };
        })
        .get<DirObject>()
    )
    .return(({ files }) => files);
};

export const getRecentUploads = (thresholdDate: Date) => (
  directories: DirObject[]
) => {
  const byDate: Ord<DirObject> = contramap(
    (dir: DirObject) => dir.lastModified
  )(ordDate);

  return pipe(
    directories,
    A.filter((dir) => dir.lastModified >= thresholdDate),
    A.sort(reverse(byDate))
  );
};

export const filterByTitle = (title: string) => (directories: DirObject[]) => {
  const byRanking = (arg: string) =>
    fromCompare<DirObject>((x, y) => {
      const distX = diceSimilarity(x.name, arg);
      const distY = diceSimilarity(y.name, arg);
      return distX === distY ? 0 : distX > distY ? -1 : 1;
    });

  return pipe(directories, A.sort(byRanking(title)));
};

export const paginateResults = ({ limit, skip }: PaginationOptions) => (
  dirs: DirObject[]
) => pipe(dirs, A.dropLeft(skip), A.takeLeft(limit));

export const filterResults = ({ nodeType, resolution }: FilteringOptions) => (
  dirs: DirObject[]
) =>
  pipe(
    dirs,
    A.filter((dirs) =>
      nodeType === "file"
        ? dirs.isFile === true
        : nodeType === "folder"
        ? dirs.isFile === false
        : dirs.isFile != null
    ),
    A.filter((dirs) => {
      const mediaRes = dirs.meta?.resolution;
      const acceptable = resolution;
      if (
        mediaRes != null &&
        ((typeof acceptable === "string" && acceptable === mediaRes) ||
          (Array.isArray(acceptable) && acceptable.includes(mediaRes)))
      )
        return true;
      else if (resolution === "all" || resolution === []) {
        return true;
      } else return false;
    })
  );
