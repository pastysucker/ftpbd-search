export const headers = {
  "X-Emby-Token": "2bee223b247c40969508d7cb13166923",
} as const;
export const userId = "1bbda9d8543e484499096a7feea95679" as const;
export const baseUrl = "http://media.ftpbd.net:8096" as const;

export const _userEndpoint = `${baseUrl}/emby/Users/${userId}` as const;
export const _seriesEndpoint = `${baseUrl}/emby/Shows` as const;

export enum CollectionId {
  EnglishSeries = "310e6a059a33254ee2e65bc4c98e8e42",
  EnglishMovies = "fb6af48929bdab3445c3dc034d5dc92c",
}
