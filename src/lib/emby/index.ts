import { fetch } from "cross-fetch";
import * as E from "fp-ts/Either";
import { pipe } from "fp-ts/function";
import * as O from "fp-ts/Option";
import * as TE from "fp-ts/TaskEither";
import { ImageUris } from "../types";
import { baseUrl, userId, headers } from "./constants";
import { mapHostPathToEmbyPath } from "./utils";

export function fetchMediaIdFromHref(
  href: string,
  mediaType: string
): TE.TaskEither<Error, string> {
  return pipe(
    mapHostPathToEmbyPath(href, mediaType),
    O.fold(
      () => TE.throwError(new Error("Could not map path")),
      (mappedPath) =>
        pipe(
          TE.tryCatch(
            () =>
              fetch(
                `${baseUrl}/emby/Users/${userId}/Items?` +
                  new URLSearchParams({
                    Recursive: "true",
                    Fields: "Path",
                    EnableImages: "true",
                    Path: mappedPath,
                  }),
                { headers }
              ),
            E.toError
          ),
          TE.map((a) => {
            a.text().then(console.log);
            return a;
          }),
          TE.chain((res) => TE.tryCatch(() => res.json(), E.toError)),
          TE.chainEitherK((json) =>
            E.fromNullable(new Error("Emby: Id could not be fetched"))(
              json["Items"][0]["Id"] as string
            )
          )
        )
    )
  );
}

export function fetchMediaIdByNameAndYear(name: string, year: string) {
  return pipe(
    TE.tryCatch(
      () =>
        fetch(
          `${baseUrl}/emby/Users/${userId}/Items?` +
            new URLSearchParams({
              Recursive: "true",
              IncludeItemTypes: "Movie,Series",
              searchTerm: name,
              Years: year,
              IncludePeople: "false",
              IncludeMedia: "true",
            }),
          { headers }
        ),
      E.toError
    ),
    TE.chain((res) => TE.tryCatch(() => res.json(), E.toError)),
    TE.chainEitherK((json) => {
      console.log("Request made for media id");
      return E.tryCatch(
        () => json["Items"][0]["Id"] as string,
        () => new Error("Emby: Id could not be found")
      );
    })
  );
}

export function fetchImagesFromId(id: string) {
  const buildImageUrl = (imageType: string) =>
    `${baseUrl}/emby/Items/${id}/Images/${imageType}`;

  return pipe(
    TE.tryCatch(
      () => fetch(`${baseUrl}/emby/Items/${id}/Images`, { headers }),
      E.toError
    ),
    TE.chain((res) => TE.tryCatch(() => res.json(), E.toError)),
    TE.map((json) => {
      let obj: ImageUris = {};
      for (const i of json as any[]) {
        if (i["ImageType"] === "Primary")
          obj.primary = buildImageUrl(i["ImageType"]);
        if (i["ImageType"] === "Backdrop")
          obj.backdrop = buildImageUrl(i["ImageType"]);
        if (i["ImageType"] === "Thumb")
          obj.thumb = buildImageUrl(i["ImageType"]);
      }
      return obj;
    })
  );
}

export * from "./utils";
