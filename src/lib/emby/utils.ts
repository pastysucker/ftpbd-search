import * as O from "fp-ts/Option";
import { baseUrl } from "./constants";
import { ImageUris } from "../types";

export const mapHostPathToEmbyPath = (
  hostPath: string,
  type: string
): O.Option<string> => {
  const url = new URL(hostPath);
  switch (type) {
    case "englishSeries":
      return O.some("/bnet-server-4/FILE--SERVER" + url.pathname);
    case "englishMovies":
      return O.some("/bnet-server-2/FILE--SERVER" + url.pathname);
    default:
      return O.none;
  }
};

export const mapEmbyPathToMediaHost = (embyPath: string): string => {
  return embyPath
    .replace(/\/bnet-server-1\/FILE--SERVER/, "http://server1.ftpbd.net")
    .replace(/\/bnet-server-2\/FILE--SERVER/, "http://server2.ftpbd.net")
    .replace(/\/bnet-server-3\/FILE--SERVER/, "http://server3.ftpbd.net")
    .replace(/\/bnet-server-4\/FILE--SERVER/, "http://server4.ftpbd.net");
  // "/bnet-server-2/FILE--SERVER/FTP-2/English Movies/Dual-Audio/2015 & Before/King Kong (2005)1080p BluRay [Hindi - English]/King Kong (2005) TC 1080p BluRay x264 Dual Audio [Hindi DD5.1 640 Kbps - English DD5.1] - ESUBS ~ Ranvijay.mkv"
  // "/bnet-server-3/FILE--SERVER/FTP-3/Foreign Language Movies/Korean Language/Parasite (2019) Hindi Dubbed BluRay 720p/Parasite (2019) 720p BluRay Org Auds [Hindi DD5.1 + Kor] 1.2GB ESub.mkv"
  // "/bnet-server-4/FILE--SERVER/FTP-4/English & Foreign TV Series/Brooklyn Nine-Nine (TV Series 2013 ) 720p"
};

enum ImageQueries {
  Primary = "quality=60&maxHeight=500",
  Backdrop = "quality=50&maxHeight=1080",
  Thumb = "quality=50&maxHeight=500",
}

const buildImageUrl = (
  id: string,
  imageType: "Primary" | "Backdrop" | "Thumb",
  imageTag: string
) =>
  `${baseUrl}/emby/Items/${id}/Images/${imageType}?tag=${imageTag}&${ImageQueries[imageType]}`;

export const mapToImageUris =
  (imageTags: Record<string, string>, backdropTags?: string[]) =>
  (id: string): ImageUris => {
    const imageUris: ImageUris = {};
    if (imageTags.hasOwnProperty("Primary")) {
      imageUris.primary = buildImageUrl(id, "Primary", imageTags["Primary"]);
    }
    if (backdropTags != null && backdropTags.length !== 0) {
      imageUris.backdrop = buildImageUrl(id, "Backdrop", backdropTags[0]);
    }
    if (imageTags.hasOwnProperty("Thumb")) {
      imageUris.thumb = buildImageUrl(id, "Thumb", imageTags["Thumb"]);
    }

    return imageUris;
  };
