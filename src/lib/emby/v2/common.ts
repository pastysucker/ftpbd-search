import { fetchJSON } from "fp-fetch";
import { pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import { tap } from "../../common";
import { SearchResult, MediaSource } from "../../types";
import { baseUrl, CollectionId, headers, _userEndpoint } from "../constants";
import { parseMediaSources, parseResultFromJson } from "./parser";

export function getRawItem(
  id: string
): TE.TaskEither<Error, Record<string, unknown>> {
  return fetchJSON<Error, any>(`${_userEndpoint}/Items/${id}?Fields=Path`, {
    headers,
  });
}

export function searchItems(
  query: string,
  itemType: "Movie" | "Series",
  limit = 6,
  skip = 0
) {
  return pipe(
    fetchJSON<Error, any>(
      `${_userEndpoint}/Items?` +
        new URLSearchParams({
          Fields: "ProductionYear,Status",
          searchTerm: query,
          IncludeItemTypes: itemType,
          IncludePeople: "false",
          EnableUserData: "false",
          Limit: String(limit),
          StartIndex: String(skip),
          Recursive: "true",
        }),
      {
        headers,
      }
    ),
    TE.chainEitherK((json) => parseResultFromJson(json["Items"]))
  );
}

export function getLatestItems(
  itemType: "Movie" | "Series",
  collectionId: CollectionId,
  limit = 6,
  skip = 0
): TE.TaskEither<Error, SearchResult[]> {
  return pipe(
    fetchJSON<Error, any>(
      `${_userEndpoint}/Items/Latest?` +
        new URLSearchParams({
          Fields: "ProductionYear",
          Limit: String(limit),
          StartIndex: String(skip),
          ParentId: collectionId,
          // IncludeItemTypes: itemType,
          EnableUserData: "false",
          Recursive: "true",
        }),
      { headers }
    ),
    TE.chainEitherK(parseResultFromJson)
  );
}

export function getPlaybackInfo(
  id: string
): TE.TaskEither<Error, MediaSource[]> {
  return pipe(
    fetchJSON<Error, any>(`${baseUrl}/emby/Items/${id}/PlaybackInfo`, {
      headers,
    }),
    tap(() => console.log(`Emby API - Requested playback info for item ${id}`)),
    TE.chainEitherK((json) => parseMediaSources(json["MediaSources"]))
  );
}
