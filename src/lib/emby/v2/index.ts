import { fetch } from "cross-fetch";
import * as E from "fp-ts/Either";
import { pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import { ImageUris } from "../../types";

// export function fetchImagesFromId(id: string) {
//   const buildImageUrl = (imageType: string) =>
//     `${baseUrl}/emby/Items/${id}/Images/${imageType}`;

//   return pipe(
//     TE.tryCatch(
//       () => fetch(`${baseUrl}/emby/Items/${id}/Images`, { headers }),
//       E.toError
//     ),
//     TE.chain((res) => TE.tryCatch(() => res.json(), E.toError)),
//     TE.map((json) => {
//       console.log("Request made for images");
//       let obj: ImageUris = {};
//       for (const i of json as any[]) {
//         if (i["ImageType"] === "Primary")
//           obj.poster = buildImageUrl(i["ImageType"]);
//         if (i["ImageType"] === "Backdrop")
//           obj.backdrop = buildImageUrl(i["ImageType"]);
//         if (i["ImageType"] === "Thumb")
//           obj.thumb = buildImageUrl(i["ImageType"]);
//       }
//       return obj;
//     })
//   );
// }

export * from "../utils";
export * from "./common";
export * from "./series";
export * from "./movie";
