import { pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import { getRawItem } from "./common";
import { parseMovieFromJson } from "./parser";

export function getMovie(id: string) {
  return pipe(getRawItem(id), TE.chainEitherK(parseMovieFromJson));
}
