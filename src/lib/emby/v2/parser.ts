import { ParserError } from "fp-fetch";
import * as A from "fp-ts/Array";
import * as TE from "fp-ts/TaskEither";
import * as E from "fp-ts/Either";
import { pipe } from "fp-ts/function";
import {
  Episode,
  MediaSource,
  Movie,
  SearchResult,
  Season,
  Series,
} from "../../types";
import { mapEmbyPathToMediaHost, mapToImageUris } from "../utils";
import { getPlaybackInfo } from "./common";

export function parseMediaSources(
  sources: any[]
): E.Either<Error, MediaSource[]> {
  if (sources != null && sources.length !== 0) {
    return E.of(
      sources.map<MediaSource>((source) => {
        const streams = (source["MediaStreams"] as any[]).filter(
          (stream) => stream["Type"] === "Video"
        );
        return {
          streamUri: mapEmbyPathToMediaHost(source["Path"]),
          bitrate: Number(source["Bitrate"]),
          fileName: source["Name"],
          fileSize: Number(source["Size"]),
          displayName: streams[0]["DisplayTitle"],
        };
      })
    );
  } else return E.throwError(new Error("No sources found"));
}

export function parseMovieFromJson(
  json: Record<string, any>
): E.Either<Error, Movie> {
  if (json["Type"] === "Movie") {
    return pipe(
      parseMediaSources(json["MediaSources"]),
      E.map((sources) => {
        const movie: Movie = {
          id: json["Id"],
          title: json["Name"],
          year: json["ProductionYear"],
          synopsis: json["Overview"],
          runtime: (json["RunTimeTicks"] as number) / 10000,
          providerIds: {
            imdb: json["ProviderIds"]["Imdb"],
            tmdb: json["ProviderIds"]["Tmdb"],
          },
          genres: json["Genres"],
          ageRating: json["OfficialRating"],
          criticRatings: {
            rottenTomatoes: json["CriticRating"],
          },
          mediaSources: sources,
          imageUris: mapToImageUris(
            json["ImageTags"],
            json["BackdropImageTags"]
          )(json["Id"]),
        };
        return movie;
      })
    );
  } else
    return E.throwError<Error, Movie>(new ParserError("Not an id of a movie"));
}

export function parseSeriesFromJson(
  json: Record<string, any>
): E.Either<Error, Series> {
  if (json["Type"] === "Series") {
    const series: Series = {
      id: json["Id"],
      title: json["Name"],
      year: json["ProductionYear"],
      synopsis: json["Overview"],
      averageRuntime:
        json["RunTimeTicks"] != null
          ? (json["RunTimeTicks"] as number) / 10000
          : undefined,
      providerIds: {
        imdb: json["ProviderIds"]["Imdb"],
        tmdb: json["ProviderIds"]["Tmdb"],
      },
      criticRatings: {
        community: json["CommunityRating"],
      },
      genres: json["Genres"],
      ageRating: json["OfficialRating"],
      endDate: json["EndDate"] != null ? new Date(json["EndDate"]) : undefined,
      hasEnded: json["Status"] != null ? json["Status"] === "Ended" : undefined,
      imageUris: mapToImageUris(
        json["ImageTags"],
        json["BackdropImageTags"]
      )(json["Id"]),
    };
    return E.of(series);
  } else return E.throwError(new ParserError("Not a series"));
}

export function parseResultFromJson(
  json: any[]
): E.Either<Error, SearchResult[]> {
  if (Array.isArray(json)) {
    return E.of(
      json.map<SearchResult>((item) => {
        return {
          id: item["Id"],
          name: item["Name"],
          imageUris: mapToImageUris(
            item["ImageTags"],
            item["BackdropImageTags"]
          )(item["Id"]),
          year: Number(item["ProductionYear"]),
          isMovie:
            item["Type"] === "Movie"
              ? true
              : item["Type"] === "Series"
              ? false
              : false,
        };
      })
    );
  } else
    return E.throwError<Error, SearchResult[]>(
      new ParserError("Could not parse results")
    );
}

export function parseSeasons(
  json: Record<string, any>
): E.Either<Error, Season[]> {
  if (
    json["Items"] != null &&
    Array.isArray(json["Items"]) &&
    json["Items"].length !== 0
  ) {
    return E.of(
      json["Items"].map<Season>((item) => {
        return {
          id: item["Id"],
          seriesId: item["SeriesId"],
          index: item["IndexNumber"],
          name: item["Name"],
          imageUris: mapToImageUris(item["ImageTags"])(item["Id"]),
        };
      })
    );
  } else return E.throwError(new ParserError("Could not parse seasons"));
}

export function parseEpisodes(
  json: Record<string, any>
): E.Either<Error, Episode[]> {
  if (
    json["Items"] != null &&
    Array.isArray(json["Items"]) &&
    json["Items"].length !== 0
  ) {
    const items = json["Items"].map<Episode>((item) => {
      return {
        id: item["Id"],
        seriesId: item["SeriesId"],
        seasonId: item["SeasonId"],
        name: item["Name"],
        index: item["IndexNumber"],
        synopsis: item["Overview"],
        runtime: Number(item["RunTimeTicks"] / 10000),
        airDate:
          item["PremiereDate"] != null
            ? new Date(item["PremiereDate"])
            : undefined,
        imageUris: mapToImageUris(item["ImageTags"])(item["Id"]),
      };
    });
    return E.of(items);
  } else {
    return E.throwError(new Error("No episodes available"));
  }
}
