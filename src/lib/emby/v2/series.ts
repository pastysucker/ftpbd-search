import { fetchJSON } from "fp-fetch";
import { pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import { Episode, Season, Series } from "../../types";
import { headers, userId, _seriesEndpoint } from "../constants";
import { getRawItem } from "./common";
import { parseSeriesFromJson, parseSeasons, parseEpisodes } from "./parser";

export function getSeries(id: string): TE.TaskEither<Error, Series> {
  return pipe(getRawItem(id), TE.chainEitherK(parseSeriesFromJson));
}

export function getSeasons(seriesId: string): TE.TaskEither<Error, Season[]> {
  return pipe(
    fetchJSON<Error, any>(
      `${_seriesEndpoint}/${seriesId}/Seasons?` +
        new URLSearchParams({ UserId: userId }),
      { headers }
    ),
    TE.chainEitherK(parseSeasons)
  );
}

export function getEpisodes(
  seriesId: string,
  seasonId: string
): TE.TaskEither<Error, Episode[]> {
  return pipe(
    fetchJSON<Error, any>(
      `${_seriesEndpoint}/${seriesId}/Episodes?` +
        new URLSearchParams({
          SeasonId: seasonId,
          IncludeItemTypes: "Episode",
          Fields: "Overview,MediaSources",
          UserId: userId,
        }),
      { headers }
    ),
    TE.chainEitherK(parseEpisodes)
  );
}
