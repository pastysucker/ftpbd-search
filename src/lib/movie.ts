import * as A from "fp-ts/Array";
import * as E from "fp-ts/Either";
import { flow, pipe } from "fp-ts/function";
import * as TE from "fp-ts/TaskEither";
import {
  filterByTitle,
  getNodes,
  getRecentUploads,
  englishMovieDomain,
} from "./common";
import {
  fetchImagesFromId,
  fetchMediaIdByNameAndYear,
  fetchMediaIdFromHref,
} from "./emby";

export const getYearDir = (year: number): TE.TaskEither<Error, string> => {
  const dirs = getNodes(
    new URL("/FTP-2/English%20Movies", englishMovieDomain),
    "movie"
  );
  return pipe(
    dirs,
    TE.map(
      flow(
        A.map((a) => a.href),
        A.findFirst((href) =>
          href.includes(year < 1995 ? "1995" : year.toString())
        )
      )
    ),
    TE.chainEitherK(E.fromOption(() => new Error("Invalid year provided")))
  );
};

const filterMoviesByTitle = (title: string, dir: string) =>
  pipe(
    getNodes(new URL(dir, englishMovieDomain), "movie"),
    TE.map(flow(filterByTitle(title)))
  );

export const filterMoviesByTitleFromYear = (title: string, year: number) =>
  pipe(
    getYearDir(year),
    TE.chain((yearDir) => filterMoviesByTitle(title, yearDir))
  );

export const getRecentMovieUploads = (thresholdDate: Date) => {
  const currentYear = new Date().getFullYear();
  return pipe(
    getNodes(
      new URL(`/FTP-2/English Movies/${currentYear}/`, englishMovieDomain),
      "movie"
    ),
    TE.map(flow(getRecentUploads(thresholdDate)))
  );
};

export const getImagesForMovie = (name: string, year: string) => {
  return pipe(
    fetchMediaIdByNameAndYear(name, year),
    TE.chain(fetchImagesFromId)
  );
};

export const getMovieFiles = (movieDir: string) =>
  getNodes(new URL(movieDir, englishMovieDomain), "movie");
