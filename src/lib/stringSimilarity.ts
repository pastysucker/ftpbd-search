export function diceSimilarity(fst: string, snd: string) {
  let i, j, k, map, match, ref, ref1, sub;
  if (fst.length < 2 || fst.length < 2) {
    return 0;
  }
  map = new Map();
  for (
    i = j = 0, ref = fst.length - 2;
    0 <= ref ? j <= ref : j >= ref;
    i = 0 <= ref ? ++j : --j
  ) {
    sub = fst.substr(i, 2);
    if (map.has(sub)) {
      map.set(sub, map.get(sub) + 1);
    } else {
      map.set(sub, 1);
    }
  }
  match = 0;
  for (
    i = k = 0, ref1 = snd.length - 2;
    0 <= ref1 ? k <= ref1 : k >= ref1;
    i = 0 <= ref1 ? ++k : --k
  ) {
    sub = snd.substr(i, 2);
    if (map.get(sub) > 0) {
      match++;
      map.set(sub, map.get(sub) - 1);
    }
  }
  return (2.0 * match) / (fst.length + snd.length - 2);
}

// export function similarity(s1: string, s2: string) {
//   var longer = s1;
//   var shorter = s2;
//   if (s1.length < s2.length) {
//     longer = s2;
//     shorter = s1;
//   }
//   var longerLength = longer.length;
//   if (longerLength == 0) {
//     return 1.0;
//   }
//   return (longerLength - editDistance(longer, shorter)) / longerLength;
// }

// function editDistance(s1: string, s2: string) {
//   s1 = s1.toLowerCase();
//   s2 = s2.toLowerCase();

//   var costs = new Array();
//   for (var i = 0; i <= s1.length; i++) {
//     var lastValue = i;
//     for (var j = 0; j <= s2.length; j++) {
//       if (i == 0) costs[j] = j;
//       else {
//         if (j > 0) {
//           var newValue = costs[j - 1];
//           if (s1.charAt(i - 1) != s2.charAt(j - 1))
//             newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
//           costs[j - 1] = lastValue;
//           lastValue = newValue;
//         }
//       }
//     }
//     if (i > 0) costs[s2.length] = lastValue;
//   }
//   return costs[s2.length];
// }

function _min(d0: number, d1: number, d2: number, bx: number, ay: number) {
  return d0 < d1 || d2 < d1
    ? d0 > d2
      ? d2 + 1
      : d0 + 1
    : bx === ay
    ? d1
    : d1 + 1;
}

export function levenshteinDistance(a: string, b: string) {
  if (a === b) {
    return 0;
  }

  if (a.length > b.length) {
    let tmp = a;
    a = b;
    b = tmp;
  }

  let la = a.length;
  let lb = b.length;

  while (la > 0 && a.charCodeAt(la - 1) === b.charCodeAt(lb - 1)) {
    la--;
    lb--;
  }

  let offset = 0;

  while (offset < la && a.charCodeAt(offset) === b.charCodeAt(offset)) {
    offset++;
  }

  la -= offset;
  lb -= offset;

  if (la === 0 || lb < 3) {
    return lb;
  }

  let x = 0;
  let y: number;
  let d0: number;
  let d1: number;
  let d2: number;
  let d3: number;
  let dd: number | undefined;
  let dy: number;
  let ay: number;
  let bx0: number;
  let bx1: number;
  let bx2: number;
  let bx3: number;

  let vector = [];

  for (y = 0; y < la; y++) {
    vector.push(y + 1);
    vector.push(a.charCodeAt(offset + y));
  }

  let len = vector.length - 1;

  for (; x < lb - 3; ) {
    bx0 = b.charCodeAt(offset + (d0 = x));
    bx1 = b.charCodeAt(offset + (d1 = x + 1));
    bx2 = b.charCodeAt(offset + (d2 = x + 2));
    bx3 = b.charCodeAt(offset + (d3 = x + 3));
    dd = x += 4;
    for (y = 0; y < len; y += 2) {
      dy = vector[y];
      ay = vector[y + 1];
      d0 = _min(dy, d0, d1, bx0, ay);
      d1 = _min(d0, d1, d2, bx1, ay);
      d2 = _min(d1, d2, d3, bx2, ay);
      dd = _min(d2, d3, dd, bx3, ay);
      vector[y] = dd;
      d3 = d2;
      d2 = d1;
      d1 = d0;
      d0 = dy;
    }
  }

  for (; x < lb; ) {
    bx0 = b.charCodeAt(offset + (d0 = x));
    dd = ++x;
    for (y = 0; y < len; y += 2) {
      dy = vector[y];
      vector[y] = dd = _min(dy, d0, dd, bx0, vector[y + 1]);
      d0 = dy;
    }
  }

  return dd;
}
