import {
  englishSeriesDomain,
  filterByTitle,
  getNodes,
  getRecentUploads,
} from "./common";
import * as TE from "fp-ts/TaskEither";
import { pipe, flow } from "fp-ts/function";

export const filterSeriesByTitle = (title: string) =>
  pipe(
    getNodes(
      new URL("/FTP-4/English %26 Foreign TV Series/", englishSeriesDomain),
      "tv"
    ),
    TE.map(flow(filterByTitle(title)))
  );

export const getRecentTvUploads = (thresholdDate: Date) => {
  return pipe(
    getNodes(
      new URL(`/FTP-4/English & Foreign TV Series/`, englishSeriesDomain),
      "tv"
    ),
    TE.map(flow(getRecentUploads(thresholdDate)))
  );
};

export const getTvFiles = (dir: string) =>
  getNodes(new URL(dir, englishSeriesDomain), "tv");
