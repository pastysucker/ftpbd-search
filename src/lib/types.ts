import {
  Edition,
  ParsedFilename,
  Resolution,
  Source,
} from "@ctrl/video-filename-parser";
import * as t from "io-ts";
import { fromNullable, NumberFromString } from "io-ts-types";

export interface DirObject {
  name: string;
  href: string;
  isFile: boolean;
  lastModified: Date;
  fileSize?: number;
  meta?: ParsedFilename | ParsedFolder /*& {
    images?: Image[];
  };*/;
}

export interface MediaSource {
  streamUri: string;
  bitrate: number;
  fileSize: number;
  fileName: string;
  displayName: string;
}

export interface Media {
  id: string;
  title?: string;
  year?: number;
  providerIds?: {
    tmdb?: string;
    imdb?: string;
  };
  genres?: string[];
  ageRating?: string;
  synopsis?: string;
  imageUris?: ImageUris;
}

export interface Movie extends Media {
  runtime: number; // in millis
  mediaSources: MediaSource[];
  criticRatings: {
    rottenTomatoes?: number; // percentage
  };
}

export interface Series extends Media {
  averageRuntime?: number;
  hasEnded?: boolean;
  endDate?: Date;
  criticRatings: {
    community?: number; // percentage
  };
}

export interface SearchResult {
  id: string;
  name: string;
  year?: number;
  imageUris?: ImageUris;
  isMovie: boolean;
}

export interface Season {
  id: string;
  seriesId: string;
  index: number;
  name: string;
  imageUris?: ImageUris;
}

export interface Episode {
  id: string;
  seriesId: string;
  seasonId: string;
  index: number;
  name: string;
  synopsis?: string;
  runtime: number;
  airDate?: Date;
  imageUris?: ImageUris;
  // mediaSources?: MediaSource[];
}

export interface ParsedFolder {
  title?: string;
  year?: string;
  seasons?: number[];
  resolution?: Resolution;
  dubbing?: string;
  sources?: Source[];
  edition?: Edition;
}

// export type Payload = {
//   status: "OK" | "Error";
//   payload: DirObject[] | FileObject[] | string;
// }

export type ResponseBody<T = unknown> =
  | { payload: T }
  | {
      error: {
        message: string;
        status?: number;
      };
    };

export type ImageUris = {
  primary?: string;
  backdrop?: string;
  thumb?: string;
};

export type SortedOptions<T> = {
  reverse: boolean;
  key: <U extends T>(arg: T) => arg is U;
};

export const paginationSchema = t.type({
  limit: fromNullable(NumberFromString, 5),
  skip: fromNullable(NumberFromString, 0),
});
export type PaginationOptions = t.TypeOf<typeof paginationSchema>;

export const filteringSchema = t.type({
  nodeType: fromNullable(
    t.union([t.literal("file"), t.literal("folder"), t.literal("mixed")]),
    "mixed"
  ),
  resolution: fromNullable(t.union([t.array(t.string), t.string]), "all"),
});
export type FilteringOptions = t.TypeOf<typeof filteringSchema>;
