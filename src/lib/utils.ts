import * as fs from "fs";
import * as path from "path";
import { Observable } from "rxjs";

/**
 * Returns an Observable of a file buffer
 */
export const readFile = (basePath: string) => (dir: string) =>
  new Observable<Buffer>((subscriber) => {
    const pathname = path.resolve(basePath, dir);

    fs.readFile(pathname, (err, file) => {
      if (err && err.code === "ENOENT") {
        subscriber.error(err);
      }
      subscriber.next(file);
      subscriber.complete();
    });
  });

export function isFile(str: string) {
  const regex = new RegExp(/\.[a-z0-9]{2,4}$/i);
  return regex.test(str);
}

/**
 * Convert `1 KB` into `1000`
 */
export function kbToBytes(str: string): number {
  return Number(str.slice(0, -3)) * 1000;
}
