import { HttpMiddlewareEffect } from "@marblejs/core";
import { tap, map } from "rxjs/operators";

const logger$: HttpMiddlewareEffect = (req$) =>
  req$.pipe(
    tap((req) =>
      console.log(
        `${new Date().toISOString()} -> ${
          req.headers["x-forwarded-for"] ?? req.socket.remoteAddress
        } ${req.method} ${req.url}`
      )
    )
  );

const cors$: HttpMiddlewareEffect = (req$) =>
  req$.pipe(
    map((v) => {
      v.response.setHeader("access-control-allow-origin", "*");
      return v;
    })
  );

const jsonUtf8: HttpMiddlewareEffect = (req$) =>
  req$.pipe(
    map((v) => {
      v.response.setHeader("Content-Type", "application/json; charset=UTF-8");
      return v;
    })
  );

export { logger$, cors$, jsonUtf8 };
